---
title: It's been a long week
date: 2021-10-02
draft: false
---

Working over 10 hours days every day, getting through this last bit of work, I needed a break. 

I have decided the next task for me to do is to work on the steam API, as I have been in the coding mode since this linux machine I have is kicking so much ass. 

Though, I want to put a couple posts up here about pandas and kedro, some pipelining tools on here.... and also get a comment section up here for the like two people that visit, and also spin up an IRC server to... well... chat about stuff for fun, or not, whatever, we'll see what happens. 

Here's what's currently on my board - 
 
- [ ] Comment code for blog
- [ ] Clean Garage
- [ ] Speed up column checking (good pandas talk)
- [ ] Switch out silent fans on computer
- [ ] Trim bushes outside
- [ ] Data for crime stats (a fun philisophical conversation I've been having with my wife the past few days)
- [ ] Make a little shelf in garage
- [ ] add metrics to my pipelines at work (or at home, either or)
- [ ] Play some video games (oh yea)
- [ ] Get next maintaince required for all vehicales in the house
- [ ] IRC server
- [ ] 3d print some keyboard shelves
- [ ] practice soldering (for the flight stick)

So, I've got my weekend planned