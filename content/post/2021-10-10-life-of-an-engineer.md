---
title: Life of an Engineer
date: 2021-10-10
draft: true 
---

This of it this way - you want to make a table. So, you begin.

Well, not making the table, that's not taking into account many different things - 
 - What is this table going to be used for? 
 - Is this for fun? Or do I need it for some reason?
 - Where will this table be used?
 - How big should it be?
 - Ornate or utilitarian?
 - Wood or metal?
 - Today or... whenever?
 - With endless other tweaks
 - Natural or colored?
 

So you begin. But where? Anyone could start at any one of those places, 
