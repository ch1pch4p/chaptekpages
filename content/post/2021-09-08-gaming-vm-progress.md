---
title: Gaming VM Progress
date: 2021-09-08
draft: false 
---

Holy crap - it's so close. I just need to get the networking to route correctly, and I think that did it. I followed [this awesome writeup](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF) and it absolutely worked. Here's the info on my machine if you are interested:


So I loaded it, and checked on my desktop, and this summary here  looks good in the post. Went to my mobile device... oof... 

Also, it spilled into the summary earlier, so I'm just buffering with spaces for now... perhaps theres a way make hugo be a little nicer? Add a tag to stop rendering the summary?


```
➜  /mnt neofetch
██████████████████  ████████   chapmana@PANZER-II 
██████████████████  ████████   ------------------ 
██████████████████  ████████   OS: Manjaro Linux x86_64 
██████████████████  ████████   Host: X570 AORUS ELITE -CF 
████████            ████████   Kernel: 5.13.13-1-MANJARO 
████████  ████████  ████████   Uptime: 1 hour, 15 mins 
████████  ████████  ████████   Packages: 1186 (pacman) 
████████  ████████  ████████   Shell: zsh 5.8 
████████  ████████  ████████   Resolution: 1920x1080, 3440x1440 
████████  ████████  ████████   DE: Xfce 4.16 
████████  ████████  ████████   WM: Xfwm4 
████████  ████████  ████████   WM Theme: Matcha-dark-aliz 
████████  ████████  ████████   Theme: Matcha-sea [GTK2], Adwaita [GTK3] 
████████  ████████  ████████   Icons: Papirus-Maia [GTK2], Adwaita [GTK3] 
                               Terminal: xfce4-terminal 
                               Terminal Font: Monospace 12 
                               CPU: AMD Ryzen 5 3600 (12) @ 3.600GHz 
                               GPU: NVIDIA GeForce GTX 1080 
                               GPU: NVIDIA GeForce GTX 660 
                               Memory: 2448MiB / 32049MiB 

```                                                    
