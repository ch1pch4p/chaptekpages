---
title: New Direction (no, not the band)
date: 2021-10-05
draft: false 
---

I think I want this site to have a direction that isn't just my to do list or moments of glory. 

So, I'm thinking I'm going to start tagging some of these articles - 

 - Tech: This ideally will have things I learned, implemented, or just want to share about tech. It will try to be original, but may not be. 

 - Personal: Anything personal I want to write about. A new haircut, a huge bowel movement, or even the weather. 

Gonna have to learn how to do that, make sure the site is ready for it, and give it a whirl!

  
