---
title: Flask API to start
date: 2021-01-05
---

So I made an api with flask (followed another tutorial) but I think this is the spring board to an API I have been wanting to make for a while. It will become more clear as I build it out. Might make some money! If I think it does, I may have to lock it down... or at least fork the important stuff into another repo. 

Anyways, it's [here](https://gitlab.com/ch1pch4p/chappy-bot). It's pretty lame, but you can call one endpoint and get a joke! 

That's all I got for now. Played some Apex with a buddy and am down a couple pounds (shooting for 200) so it's all a work in progress :-) 
