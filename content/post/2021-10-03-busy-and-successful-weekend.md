---
title: Busy and successful weekend
date: 2021-10-03
draft: false 
---

Well I certianly got through a lot - most of all moved my CPU out of my closet office and into the main room, and here's hoping it keeps it cooler.. we'll find out!

I got a set of brackets printed for my keyboards, and IRC server spun up, a couple of blog posts, got some work done for tomorrow (it was actually a bit of fun) and....

Today Ryker was the best kid we could have asked for! I would do a motion with my arms, pumping them out in front of me like a zumba class, but really fast and just about a foot over his face while he lied down... and he laughed. so. hard. It made my heart swell. It was fantastic. 

I had a few friends that being a parent you don't get to have the perfect day just how you want it anymore, but rather you get to live in these incredible moments. That was one of them, and I hope I don't forget it anytime soon. 

Anyways, I think I have more things to do on this machine now (WHAT??? I know, fuck me) 

- [ ] Let's Encrypt to get ssl certs on all my sites
- [ ] Backups of my servers
- [ ] Change the OS to Cent for my web server (I'm running Ubuntu 18 Workstation [in my defense I ran it as a desktop for a while, but was lazy and didn't change it back]) 
- [ ] Set up my old desktop into a server of some vareity (thinking I will bring it to texas and set it up there for remote access) 
  - Hmm... actually, I think if I bring a rasberri PI there, that should be more than enough, and would give me alot of flexibility on their network. 
    - Holy shit I just looked and they are either impossible to find, or wicked expensive. Over 100 bucks for a 35 dollar peice of hardware? The pandemic strikes another sector. 

I'll be honest, in the middle of writing this, I went and took a shower. Damn that felt good. I also totally lost my train of thought. So for now, I'm gonna wrap things up here, and then I'll be back later.

