---
title: Getting through my projects
date: 2021-09-06
---

So, it's been a while, but I have alot to update on. 
I'm a dad now - idk if I put that in my last post, but it's reeeeeal! Ryker is lying in my arms as I type this, so, that's fun.

But I notice I don't have a lot of time left before I truely am on full time dad mode... which means I need to wrap up my projects I have started. 
Which... I have alot - 

Side note - I tend to start projects, and only finish the ones I truely care about. Like my workbench, or my gunpla kits. But these are the ones that are in the docket, which will be coming next:

- 3D printed flight control module
- Steam game chooser
- Race car for Lemons (oh did I mention THAT'S A THING [I'll make another post about that shortly])
- Home network storage
- Gaming setup in Linux

Those are the things _I have started_ but have not completed. There are plenty of other projects I need to start on - 
- Painting house
- Flooring of house
- Crawlspace door
- several app ideas

So that's what's going on in my head, but that's certianly not all of it. I'll post up pictures once I get another free hand to upload pictures to this server. In the mean time, tata for now!
