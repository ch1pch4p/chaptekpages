---
title: Columnar operations in pandas
date: 2021-09-30
draft: true 
---

I wanted to work through a problem - if I had a dataframe that had many rows... like, many rows, but I know I will not have many columns, I figure I should treat the problem in a column fashion. 

For example, I am given a series that represents a hierarchy:
```
pd.Series(['a','b','c'])
```
In this example, a reports to b, and b reports to c.
You would have a node, and on that node, it only knows of one parent. It ends up to be a tree of data.

So, say you have this -
 
