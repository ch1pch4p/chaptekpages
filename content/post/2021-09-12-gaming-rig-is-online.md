---
title: Gaming Rig is online
date: 2021-09-12
draft: false 
---

Ok, so first off, I did something I had been avoiding for many years. It may have been more laziness than anything, but I was finally able to get this project done!

I have a linux computer with a vm of a Windows machine, but I have passed through my main graphics card to it so I can still game on it. I rarely play video games (not like I used to) but when I do, I didn't want to have to dual boot. 

For many years, I would end up getting fed up with Windows and reinstalling Linux, and I tried all sorts of flavors, cause really, why not? But I would always have the itch to play a game or two, and Proton DB (A process in steam that utilzies Wine and the like to emulate the layer to play the games) never had the performnce I could get out of a bare metal windows box. So, I would uninstall and reinstall. 

Yes, there were times I would dual boot, but that was just inconvient, and I found I would just stay in Windows because I wouldn't have to go through the hassle of closing everything, shuting down, starting up, and opening everything back up.

But now....._now_.... I don't have to choose. I can stick with my desired OS of choice, and whenever I want to game, flip push a button, change my monitor and turn on my controller, and I'm in. 

There's a few things left I'd like to do, but for now, I think I want to just enjoy this. 

- [ ] 3D printed flight control module
- [ ] Steam game chooser
- [ ] Race car for Lemons
- [X] Home network storage
- [X] Gaming setup in Linux

I'm gonna surround this list and then take on the last one, cause that requires some skills that I don't think I can learn in time... or, on the first try (welding a roll cage!)

Bonus - here's a pic of my PC with the Windows VM running (top screen) on the host (bottom screen)  
![PanzerII Linux + Gaming VM](/posts/2021/09/my_linux_win_rig_panzer_ii.jpg)

