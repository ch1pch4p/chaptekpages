---
title: OK Last one - IRC Server
date: 2021-10-02
draft: false
---

Well, I got it up and running! Wether it totally works or not, well, I was able to connect to it through my phone on a different network, so that's saying something.

I ended up taking it down, cause there's alot of security settings I still need to set up and understand, but it will be at - 

`irc.chaptek.net`

and if you want to try making one yourself, just checkout [this docker-compose](https://gitlab.com/ch1pch4p/irc_server) file that makes it super easy - 

Ok, Enough of this. More tomorrow!... maybe.