---
title: A springy cable
date: 2021-01-04
---

So Sunday is my day of rest (or, so I'm gonna keep it that way. Hell a whole religion follows that, why can't I?) 
Anyways, tonight I wanted to try something I have been eyeing for a while now, which is to have my own special coiled cable for my keyboard. 
Yes. 
I could have very well spent some good money on a new cable, but it turns out following this here [link](https://geekhack.org/index.php?topic=82202.0) does a great job! 

Now... time to figure out how to host pictures on this site... Maybe that's a later tonight thing or tomorrow thing. Idk, but had to get this down. 

Also, went on my first run of the year <tada> . And it was short, and unimpressive, and exausting, but DAMN did it feel good! Cue the joke.

Edit:
Let's see how this looks!
![Starting Supplies](/posts/2021/01/starting_supplies.png)

And the finished result!
![So hipster](/posts/2021/01/finished_cable.png)
