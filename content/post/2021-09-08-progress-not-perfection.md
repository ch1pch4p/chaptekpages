---
title: Progress not perfection
date: 2021-09-08
---

So, I made it through one of items on my list - 

- [ ] Steam game chooser
- [ ] Race car for Lemons (oh did I mention THAT'S A THING [I'll make another post about that shortly])
- [X] Home network storage

And started good progress on some others:

- [ ] Gaming setup in Linux
  - Since today, I have gotten proton DB to run Horizon Zero Dawn on my manjaro machine, which has been very exciting to see through.
  - Now, I have installed an old video card, and want to put my 1080 as a pass through to a VM, so I can run at full speed. There's a guy I work with who is a super brain on Linux who is helping me, and has really sparked my interest back into linux. Since then, I have wiped windows from all of my home devices and gone with linux. 
- [ ] 3D printed flight control module
  - The other night I worked on some soldering practice (which reminds me I need to get some more solder) and installed all the switches in the control board. I was also able to understand the library for the arduino controller for joysticks, so I hope that works out once I get a prototype going. 

It's raining really hard out right now, and the only thing I can think about when it rains is that I hope this house doesn't flood or collapse or suffer irrepariable damages. Really stuipd stuff, but nonetheless, what I'm thinking. 



