#!/usr/bin/python

import sys
import datetime

rTitle = input('enter the title: ')
x = datetime.datetime.now()

postName = x.strftime("%Y-%m-%d-")+rTitle.lower().replace(" ","-")+'.md'

fh = open("./content/post/" + postName, "w")
linesOftext = ["---\n",
            "title: " + rTitle + "\n",
            "date: " + x.strftime("%Y-%m-%d") + "\n", 
            "draft: true \n",
            "---\n", 
            "\n"]

fh.writelines(linesOftext)
fh.close()
print("./content/post/"+ postName)
